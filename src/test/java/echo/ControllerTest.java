package echo;

import static org.junit.jupiter.api.Assertions.*;

import java.io.Console;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import echo.util.JavalinApp;
import entidades.Ciclista;
import entidades.Funcionario;
import entidades.Ciclista.Nacionalidade;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;



class ControllerTest {

    private static JavalinApp app = new JavalinApp(); // inject any dependencies you might have

    @BeforeAll
    static void init() {
        app.start(7010);
    }
    
    @AfterAll
    static void afterAll(){
        app.stop();
    }

    @Test
    void getEchoTest() {
        HttpResponse response = Unirest.get("http://localhost:7010/Artur").asString();
        assertEquals(200, response.getStatus());
        assertEquals("Artur Artur Artur",response.getBody());
    }

    @Test
    void getRootTest() {
        HttpResponse response = Unirest.get("http://localhost:7010/").asString();
        assertEquals(200, response.getStatus());
        assertEquals("Isto e um eco, digite algo a mais no caminho",response.getBody());
    }

    @Test 
    void getFuncionarioTest(){
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario").header("accept", "application/json").asJson();
        assertEquals(200, response);
    }

    @Test 
    void getFuncionariobyIdTest(){
        String id = "21231232123123123";
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/"+id).asJson();
        assertEquals(200,response);
    }

    @Test
    void getPostFuncionarioTest(){
        Funcionario funcionario = new Funcionario("142342", "leojardim5@gmail.com", "Leonardo Moura", "LMsas221", 25, "programaror", "12312342322");

        HttpResponse<JsonNode> response =  Unirest.post("http://localhost:7010/funcionario/").body(funcionario).asJson();

        assertEquals(200, response);

    }

    void getCiclistaTest(){
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario").header("accept", "application/json").asJson();
        assertEquals(200, response);
    }

    @Test 
    void getCiclistabyIdTest(){
        String id = "21231232123123123";
        HttpResponse<JsonNode> response = Unirest.get("http://localhost:7010/funcionario/"+id).asJson();
        assertEquals(200,response);
    }

    @Test
    void getPostCiclistaTest(){
        Ciclista ciclista = new Ciclista("Carlos Henry","1231232231","Brasil",Nacionalidade.BRASILEIRA,"25/11/1995","carlos23@gmail.com","30/11/2025");

        HttpResponse<JsonNode> response =  Unirest.post("http://localhost:7010/funcionario/").body(ciclista).asJson();

        assertEquals(200, response);

    }
}
