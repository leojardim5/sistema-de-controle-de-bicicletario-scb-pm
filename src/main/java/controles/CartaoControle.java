package controles;
import java.net.CacheRequest;

import entidades.CartaoDeCredito;
import io.javalin.http.*;
import servicos.CartaoServico;


public class CartaoControle {
        
    private CartaoControle(){  
        throw new IllegalStateException("Utility class");
    }
    public static void getCartao(Context ctx){

        CartaoDeCredito cartao = CartaoServico.getCartao();

        if(cartao != null){
            ctx.status(200);
            ctx.json(cartao);
        }else {
            ctx.json("cartoes nao encontrados");
        }



    }
    public static void putCartao(Context ctx){
        String body = ctx.body();

        CartaoDeCredito cartao = CartaoServico.putCartao(body);

        if(cartao != null){
            ctx.status(200);
            ctx.json("cartao alterado com sucesso");
        } else {
            ctx.json("Cartao nao alterado");
        }
        
    }

}
