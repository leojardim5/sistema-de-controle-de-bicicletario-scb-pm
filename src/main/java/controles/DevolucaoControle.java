package controles;
import entidades.Devolucao;
import io.javalin.http.*;
import servicos.DevolucaoServico;

public class DevolucaoControle {
    
    private DevolucaoControle() {
    }

    public static void postDevolucao(Context ctx){

        

        String body = ctx.body();

        Devolucao devolucao = DevolucaoServico.postDevolucao(body);

        if(devolucao != null ){
            ctx.status(200);
            ctx.json("Devolução concluida");
        } else{
            ctx.json("devolucao nao feita");
        }
    }

    

}
