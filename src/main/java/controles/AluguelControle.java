package controles;

import entidades.Aluguel;
import entidades.Ciclista;
import io.javalin.http.*;
import servicos.AluguelServico;

public class AluguelControle {
    

    private AluguelControle(){
        throw new IllegalStateException("Utility class");
    }
    public static void postAluguel(Context ctx){
        
        String body = ctx.body();
        
        Aluguel aluguel = AluguelServico.postAluguel(body);

        if(aluguel != null){
            ctx.status(200);
            ctx.json("Aluguel feito com sucesso");
        } else {
            ctx.json("Aluguel nao concluido");
        }
        
    }


    
}
