package controles;

import java.util.UUID;

import entidades.Ciclista;
import io.javalin.http.*;
import servicos.CiclistaServico;


public class CiclistaControle{

    private static String idCiclista = "idCiclista"    ; 

    private CiclistaControle() {
    }

    public static void postCiclista(Context ctx){
        String body = ctx.body();
        
        Ciclista ciclista = CiclistaServico.postCiclista(body);
        
        if(ciclista != null) {
            ctx.status(200);
            ctx.json("Ciclista cadastrado");

        } else {
            ctx.json("Ciclista nao cadastrado");
        }

    }

    public static void getCiclista(Context ctx){
        String id = ctx.pathParam(idCiclista);
        Ciclista ciclista = CiclistaServico.getCiclista(id);

        if(ciclista != null){
            ctx.status(200);
            ctx.json(ciclista);

        } else {
            ctx.json("Ciclista nao encontrado");
        }
    }

    public static void putCiclista(Context ctx){
        String body = ctx.body();
        String id = ctx.pathParam(idCiclista);

        Ciclista ciclista = CiclistaServico.getCiclista(id);

        Ciclista ciclistaAlterado = CiclistaServico.putCiclista(body, ciclista);

        if(ciclistaAlterado != null){
            ctx.status(200);
            ctx.json("Ciclista Alterado");
        } else {
            ctx.json("Ciclista nao alterado");
        }

    }

    public  static void getCiclistaAtivar(Context ctx){
        String id = ctx.pathParam(idCiclista);
        Ciclista ciclista = CiclistaServico.getCiclistaAtivar(id);
        if(ciclista != null ) {
            ctx.status(200);
            ctx.json("Ciclista alterado");
        } else {
            ctx.json("Ciclista não existe");
        }
    }

    public static boolean existeEmail(Context ctx){
        String id = ctx.pathParam(idCiclista);
        if(id != null){
            ctx.status(200);
            return true;
        } else {
            return false;
        }
        
        
    }

}