package controles;
import java.util.ArrayList;
import entidades.*;
import io.javalin.http.*;
import servicos.FuncionarioServico;


public class FuncionarioControle {
    

    private FuncionarioControle(){

    }
    
    private static String idFuncionario = "idFuncionario";

    public static void getFuncionario(Context ctx){
        ArrayList<Funcionario> listaFunc = FuncionarioServico.getFuncionario();

        if(listaFunc != null){
            ctx.status(200);
            ctx.json(listaFunc);
        } else {
            ctx.status(400);
            ctx.json("Erro ,usuário não encontrado");
        }
    }

    public static void postFuncionario(Context ctx){

        String body = ctx.body();
        Funcionario funcionario = FuncionarioServico.postFuncionario(body);
        

        if(funcionario != null){
            ctx.status(200);
            ctx.json(funcionario);
        } else {
            ctx.status(422);
            ctx.json("ERRO, Funcionário não cadastrado");
        }

        

    }

    public static void getFuncionariobyId(Context ctx){
        String id = ctx.pathParam(idFuncionario);
        Funcionario funcionario = FuncionarioServico.getFuncionario(id);

        if(funcionario != null){
            ctx.status(200);
            ctx.json(funcionario);
        } else {
            ctx.status(422);
            ctx.json("Funcionario não encontrado");
        }
    }

    public static void putFuncionario(Context ctx){
        String id = ctx.pathParam(idFuncionario);
        String body = ctx.body();

        Funcionario funcionario = FuncionarioServico.getFuncionario(id);
        Funcionario funcionarioAlterado = FuncionarioServico.putFuncionario(body, funcionario);

        if(funcionarioAlterado != null){
            ctx.status(200);
            ctx.json(funcionario);
        }  else {
            ctx.status(404);
            ctx.json("Usuario não encontrado");
        }

    }

    public static void deleteFuncionario(Context ctx){
         String id = ctx.pathParam(idFuncionario);
         Funcionario funcionario = FuncionarioServico.getFuncionario(id);

         if(funcionario != null){
            FuncionarioServico.deleteFuncionario(funcionario);
            ctx.result("funcionario deletado");
            ctx.status(200);
         } else{
            ctx.status(400);
            ctx.result("usuario nao encontrado");
         }

         



    }


    


}
