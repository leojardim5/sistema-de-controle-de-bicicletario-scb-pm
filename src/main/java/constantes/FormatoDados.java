package constantes;

import java.util.regex.Pattern;

public class FormatoDados
{    
    
    public static final String  REGEX_ID = "[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}";
    public static final String  REGEX_NUMERO = "-?\\d+(\\.\\d+)?";
    public static final Pattern ID_ENTIDADE = Pattern.compile(REGEX_ID);
    public static final Pattern NUMERO_ENTIDADE = Pattern.compile(REGEX_NUMERO);
    public static final String DATA = "dd/MM/yyyy HH:mm:ss";
}
