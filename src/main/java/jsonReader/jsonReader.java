package jsonReader;
import java.io.ObjectStreamException;
import java.util.LinkedHashMap;
import java.util.Map;

import com.google.gson.*;



public class jsonReader {
    
    private jsonReader(){
        
    }

    public static Object buildJson(String variaveis,Class<Object> classe){
        
        Gson builderJson = new GsonBuilder().setPrettyPrinting().create();

        return builderJson.fromJson(variaveis, classe);
    }

    public static JsonObject dismontJson(String body){

        JsonObject jsonObj = JsonParser.parseString(body).getAsJsonObject();

        return jsonObj;
    }

    public static String errorJson(String id,String erro,String message){

        Map<String, String> lista = new LinkedHashMap<>();

        lista.put("id", id);
        lista.put("erro", erro);
        lista.put("message",message);

        String retorno = "retorno";

        return retorno;

    }
}
