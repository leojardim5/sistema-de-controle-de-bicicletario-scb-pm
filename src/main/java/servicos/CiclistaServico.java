package servicos;

import java.util.ArrayList;
import java.util.UUID;
import io.javalin.http.*;
import entidades.*;
import com.google.gson.Gson;
import entidades.*;
import servicos.*;
import controles.*;

import entidades.Ciclista;
import entidades.Ciclista.estaAtivo;

public class CiclistaServico{


    private CiclistaServico(){
    }
    private static ArrayList<Ciclista> listaCiclista = new ArrayList<>();

    public static Ciclista postCiclista(String body){
        Gson buildJson = new Gson();

        Ciclista ciclista = buildJson.fromJson(body,Ciclista.class);

        listaCiclista.add(ciclista);

        return ciclista;
    }

    public static Ciclista getCiclista(String idCiclista){
        
    
            Ciclista ciclista = listaCiclista.stream().filter(cicli -> idCiclista.equals(cicli.getIdCiclista().toString())).findAny().orElse(null);



            if(ciclista != null){
                return ciclista;
            }
        return null;

    }

    public static Ciclista putCiclista(String body, Ciclista ciclista)
    {Gson buildJson = new Gson();
        Ciclista ciclistaInfoModificada = buildJson.fromJson(body, Ciclista.class);

        if(ciclistaInfoModificada.getIdCiclista() != null){
             ciclista.setIdCiclista(ciclistaInfoModificada.getIdCiclista());
        }
        if(ciclistaInfoModificada.getEmail() != null){
            ciclista.setEmail(ciclistaInfoModificada.getEmail());
        }
        
        return ciclista;
    }

    public static Ciclista getCiclistaAtivar(String idCiclista){
        boolean taEmDia = true;
        
        Ciclista ciclista = listaCiclista.stream().filter(cicli -> idCiclista.equals(cicli.getIdCiclista().toString())).findAny().orElse(null);

        if(ciclista != null){
            ciclista.setCondicao(estaAtivo.ATIVO);
            return ciclista;
        }

        return ciclista;
    }

    public static boolean permiteAluguel(String idCiclista){
    
        for(int m = 0;m < AluguelServico.getListaAluguel().size(); m++){
            if(AluguelServico.getListaAluguel().get(m).getBicicleta().equals(idCiclista)){
                return false;
            } else {
                return true;
            }

        }
        return false;
            
    }    
    
    public static boolean existeEmail(String idCiclista){
        if(idCiclista != null){
            return true;
        } else {
            return false;
        }
        
    }

}