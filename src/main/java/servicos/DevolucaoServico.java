package servicos;
import java.net.http.HttpResponse;

import org.w3c.dom.views.DocumentView;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import entidades.Devolucao;

import jsonReader.jsonReader;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
public class DevolucaoServico {
        private DevolucaoServico(){
            
        }

    public static Devolucao postDevolucao(String body){
    
        
        JsonObject objJson = jsonReader.dismontJson(body); 
        
        String numBicicleta = objJson.get("idBicicleta").getAsString();
        String numCiclista = objJson.get("idCiclista").getAsString();
        String trancaFim = objJson.get("trancaFim").getAsString();

        String horaInicio = objJson.get("horaInicio").getAsString();

        String horaFim = objJson.get("horaFim").getAsString();

        int horaTotal = Integer.parseInt(horaFim)  - Integer.parseInt(horaInicio);

        int valorASePagar = horaTotal * 10;
        Unirest.post("https://pm-externo.herokuapp.com/cobranca").body("{"+valorASePagar+","+numCiclista+"}").asJson();


        Unirest.post("https://pm-g5-controle-bicicletario.herokuapp.com/bicicleta/"+numBicicleta+"/status/[DISPONIVEL]").asJson();

        Unirest.post("https://pm-g5-controle-bicicletario.herokuapp.com/tranca/"+trancaFim+"/trancar").asJson();

        
        Devolucao devolucao = new Devolucao(numBicicleta,horaInicio,trancaFim,horaFim,valorASePagar,numCiclista);

        Unirest.post("https://pm-externo.herokuapp.com/enviarEmail").body("{"+numCiclista+","+ devolucao+"}").asJson();

        return devolucao;
    }
}
