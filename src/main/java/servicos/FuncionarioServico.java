package servicos;
import java.util.ArrayList;
import entidades.*;
import com.google.gson.Gson;



public class FuncionarioServico {

    private FuncionarioServico(){
        
    }
    
    private static ArrayList<Funcionario> listaFuncionario = new ArrayList<>();

    public static ArrayList<Funcionario> getFuncionario(){

        return listaFuncionario;

    }

    public static Funcionario postFuncionario(String body){
        
        Gson buildJson = new Gson();
        Funcionario funcionario = buildJson.fromJson(body, Funcionario.class);

        listaFuncionario.add(funcionario);

        return funcionario;
        
    }

    public static Funcionario getFuncionario(String idFuncionario){

  
                Funcionario funcionario = listaFuncionario.stream().filter(funcio -> idFuncionario.equals(funcio.getIdFuncionario().toString())).findAny().orElse(null);

                if(funcionario != null){
                    return funcionario;
                }

            return null;
    }

    public static Funcionario putFuncionario(String body,Funcionario funcionario){

        Gson buildJson = new Gson();
        Funcionario funcionarioInfoModificada = buildJson.fromJson(body, Funcionario.class);

        if(funcionarioInfoModificada.getIdFuncionario() != null){
            funcionario.setIdFuncionario(funcionarioInfoModificada.getIdFuncionario());
        }
        if(funcionarioInfoModificada.getEmail() != null){
            funcionario.setEmail(funcionarioInfoModificada.getEmail());
        }
        if(funcionarioInfoModificada.getFuncao() != null){
            funcionario.setFuncao(funcionarioInfoModificada.getFuncao());
        }
        if(funcionarioInfoModificada.getRegistro() != null){
            funcionario.setRegistro(funcionarioInfoModificada.getRegistro());
        }
        if(funcionarioInfoModificada.getSenha() != null){
            funcionario.setSenha(funcionarioInfoModificada.getSenha());
        }
        
        return funcionario;
    }

    public static void deleteFuncionario(Funcionario funcionario){
        for(int i = 0;i<listaFuncionario.size();i++){
            if(listaFuncionario.get(i).getIdFuncionario().equals(funcionario.getIdFuncionario())){
                listaFuncionario.remove(i);
            }
        }
    }
}
