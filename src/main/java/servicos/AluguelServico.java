package servicos;
import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.Calendar;

import servicos.*;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import jsonReader.jsonReader;
import entidades.*;
import io.javalin.http.*;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;


public class AluguelServico {
    
    private static String servicoEquipamento = "https://pm-g5-controle-bicicletario.herokuapp.com/tranca/";

    private AluguelServico(){

    }

    private static ArrayList<Aluguel> listaAluguel = new ArrayList<>(); 

    public static ArrayList<Aluguel> getListaAluguel() {
        return listaAluguel;
    }

    public static Aluguel postAluguel(String body){
        
        JsonObject objJson = jsonReader.dismontJson(body); 
        
        String idTrancaInicio = objJson.get("idTrancaInicio").getAsString();
        String ciclista = objJson.get("ciclista").getAsString();
    
        

        HttpResponse<JsonNode> getBicicletaResponse = (HttpResponse<JsonNode>) Unirest.get(servicoEquipamento+idTrancaInicio+"/bicicleta").asJson();

        ;

        int cobranca = 0;
        Calendar data = Calendar.getInstance();
        int hora = data.get(Calendar.HOUR_OF_DAY); 
        Aluguel aluguel = new Aluguel(getBicicletaResponse.body().toString(),hora,idTrancaInicio,hora,cobranca,idTrancaInicio,ciclista);

        Unirest.post("https://pm-g5-controle-bicicletario.herokuapp.com/bicicleta/"+getBicicletaResponse+"/status/[EM USO]").asJson();

        Unirest.post(servicoEquipamento+idTrancaInicio+"/destrancar").asJson();


        Unirest.post("https://pm-externo.herokuapp.com/enviarEmail").body("{ciclista.getEmail(),"+ aluguel+"}").asJson();
        

        return aluguel;
    }

    // HttpResponse<JsonNode> trancaResponse = (HttpResponse<JsonNode>) Unirest.get(servicoEquipamento+idTrancaInicio).asJson();

        // if(trancaResponse == 404){
        //     return null;
        // }; 



    
}
