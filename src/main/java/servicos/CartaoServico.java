package servicos;


import com.google.gson.Gson;

import entidades.*;

public class CartaoServico {
    
    private CartaoServico(){

    }

    public static CartaoDeCredito getCartao(){
        CartaoDeCredito cartao = new CartaoDeCredito();

        return cartao;
       
    }
    public static CartaoDeCredito putCartao(String body){
        Gson buildJson = new Gson();
        CartaoDeCredito cartao = buildJson.fromJson(body, CartaoDeCredito.class);

        return cartao;
    }

}
