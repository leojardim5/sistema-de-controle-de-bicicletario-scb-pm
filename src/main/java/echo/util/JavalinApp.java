package echo.util;
import io.javalin.Javalin;
import static io.javalin.apibuilder.ApiBuilder.*;
import controles.*;
import echo.*;


public class JavalinApp {
    private Javalin app = 
            Javalin.create(config -> config.defaultContentType = "application/json")
                .routes(() -> {
                    path("/:echo", () -> get(Controller::getEcho));
                    path("/", ()-> get(Controller::getRoot));

                    path("funcionario", () ->{
                        get(FuncionarioControle::getFuncionario);
                        post(FuncionarioControle::postFuncionario);
                        put(FuncionarioControle::putFuncionario);
                        delete(FuncionarioControle::deleteFuncionario);

                        path(":idFuncionario",()->{
                            get(FuncionarioControle::getFuncionariobyId);
                            
                        });
                    });
                    path("ciclista",() -> {
                        post(CiclistaControle::postCiclista);
                        put(CiclistaControle::putCiclista);
                        path(":idCiclista",()->{
                            get(CiclistaControle::existeEmail);
                            get(CiclistaControle::getCiclista); 
                            get(CiclistaControle::getCiclistaAtivar);
                        });
                        
                        
                    });
                    path("aluguel",() -> {
                        post(AluguelControle::postAluguel);
                    });

                    path("cartao",()->{
                        put(CartaoControle::putCartao);
                        path(":idCartaoDeCredito",()->{
                            get(CartaoControle::getCartao);
                        });
                    });

                    path("devolucao",()->{
                        post(DevolucaoControle::postDevolucao);
                    });

                    });
                    


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }
}
