package entidades;

import java.time.LocalTime;
import java.util.UUID;

public class Devolucao {

    private String bicicleta;
    private String horaInicio;
    private String trancaFim;
    private String horaFim;
    private int cobranca;
    private String ciclista;

    
    public Devolucao(String bicicleta, String horaInicio, String trancaFim, String horaFim, int cobranca,
            String ciclista) {
        this.bicicleta = bicicleta;
        this.horaInicio = horaInicio;
        this.trancaFim = trancaFim;
        this.horaFim = horaFim;
        this.cobranca = cobranca;
        this.ciclista = ciclista;
    }

    public String getBicicleta() {
        return bicicleta;
    }
    public void setBicicleta(String bicicleta) {
        this.bicicleta = bicicleta;
    }
    public String getHoraInicio() {
        return horaInicio;
    }
    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }
    public String getTrancaFim() {
        return trancaFim;
    }
    public void setTrancaFim(String trancaFim) {
        this.trancaFim = trancaFim;
    }
    public String getHoraFim() {
        return horaFim;
    }
    public void setHoraFim(String horaFim) {
        this.horaFim = horaFim;
    }
    public int getCobranca() {
        return cobranca;
    }
    public void setCobranca(int cobranca) {
        this.cobranca = cobranca;
    }
    public String getCiclista() {
        return ciclista;
    }
    public void setCiclista(String ciclista) {
        this.ciclista = ciclista;
    }

    
}
