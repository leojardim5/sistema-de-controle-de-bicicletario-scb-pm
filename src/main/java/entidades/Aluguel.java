package entidades;


import java.time.LocalTime;
import java.util.UUID;

public class Aluguel {
    
    private String bicicleta;
    private int horaInicio;
    private String trancaFim;
    private int horaFim;
    private int cobranca;
    private String trancaInicio;
    private String ciclista;



    public Aluguel(String bicicleta, int horaInicio, String trancaFim, int horaFim, int cobranca,
            String trancaInicio, String ciclista) {
        this.bicicleta = bicicleta;
        this.horaInicio = horaInicio;
        this.trancaFim = trancaFim;
        this.horaFim = horaFim;
        this.cobranca = cobranca;
        this.trancaInicio = trancaInicio;
        this.ciclista = ciclista;
    }
    public String getBicicleta() {
        return bicicleta;
    }
    public void setBicicleta(String bicicleta) {
        this.bicicleta = bicicleta;
    }
    public int getHoraInicio() {
        return horaInicio;
    }
    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }
    public String getTrancaFim() {
        return trancaFim;
    }
    public void setTrancaFim(String trancaFim) {
        this.trancaFim = trancaFim;
    }
    public int getHoraFim() {
        return horaFim;
    }
    public void setHoraFim(int horaFim) {
        this.horaFim = horaFim;
    }
    public int getCobranca() {
        return cobranca;
    }
    public void setCobranca(int cobranca) {
        this.cobranca = cobranca;
    }
    public String getTrancaInicio() {
        return trancaInicio;
    }
    public void setTrancaInicio(String trancaInicio) {
        this.trancaInicio = trancaInicio;
    }
    public String getCiclista() {
        return ciclista;
    }
    public void setCiclista(String ciclista) {
        this.ciclista = ciclista;
    }

    
}
