package entidades;

import java.util.UUID;

public class Funcionario {
    
    private UUID idFuncionario;
    private String registro;
    private String email;
    private String nome;
    private String senha;
    private int idade;


    public Funcionario( String registro, String email, String nome, String senha, int idade,
            String funcao, String cpf) {

        this.idFuncionario = UUID.randomUUID();
        this.registro = registro;
        this.email = email;
        this.nome = nome;
        this.senha = senha;
        this.idade = idade;
        this.funcao = funcao;
        this.cpf = cpf;
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return super.toString();
    }
    
    private String funcao;
    private String cpf;




    public int getIdade() {
        return idade;
    }
    public void setIdade(int idade) {
        this.idade = idade;
    }
    public String getFuncao() {
        return funcao;
    }
    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }
    public UUID getIdFuncionario() {
        return idFuncionario;
    }
    public void setIdFuncionario(UUID idFuncionario) {
        this.idFuncionario = idFuncionario;
    }
    public String getRegistro() {
        return registro;
    }
    public void setRegistro(String registro) {
        this.registro = registro;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public String getSenha() {
        return senha;
    }
    public void setSenha(String senha) {
        this.senha = senha;
    }
    public String getCpf() {
        return cpf;
    }
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

}
