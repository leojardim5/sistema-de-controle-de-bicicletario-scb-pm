package entidades;

import java.time.LocalDate;
import java.util.Date;
import java.util.UUID;

public class Ciclista {
    
    private String nome;
    private Passaporte passaporte;
    private String nascimento;
    private Nacionalidade nacionalidade;
    private String email;
    private UUID idCiclista; 
    private estaAtivo condicao;
    private String pais;

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public enum estaAtivo{
        ATIVO("Ativo"),
        PENDENTE("Pendente"),
        SUSPENSO("Suspenso");

        private final String descricao;

        estaAtivo(String descricao){
            this.descricao = descricao;
        }
    }

    public Ciclista(String nome, String numeroPassaport,String pais,Nacionalidade nacionalidade, String nascimento , String email,String validade) {
        this.nome = nome;
        this.passaporte = new Passaporte(numeroPassaport,pais,validade);
        this.nascimento = nascimento;
        this.email = email;
        this.idCiclista = UUID.randomUUID();
        this.pais = pais;
        this.nacionalidade = nacionalidade;


    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return super.toString();
    }

    public UUID getIdCiclista() {
        return idCiclista;
    }

    public void setIdCiclista(UUID idCiclista) {
        this.idCiclista = idCiclista;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Passaporte getPassaporte() {
        return passaporte;
    }

    public void setPassaporte(Passaporte passaporte) {
        this.passaporte = passaporte;
    }

    public String getNascimento() {
        return nascimento;
    }

    public void setNascimento(String nascimento) {
        this.nascimento = nascimento;
    }

    public Nacionalidade getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(Nacionalidade nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public class Passaporte{
        private String numero;
        private String pais;
        private String validade;

        public String getNumero() {
            return numero;
        }

        public void setNumero(String numero) {
            this.numero = numero;
        }

        public String getPais() {
            return pais;
        }

        public void setPais(String pais) {
            this.pais = pais;
        }

        public String getValidade() {
            return validade;
        }

        public void setValidade(String validade) {
            this.validade = validade;
        }

        public Passaporte(String numero, String pais, String validade) {
            this.numero = numero;
            this.pais = pais;
            this.validade = validade;
        }
    }

    public enum Nacionalidade{
        BRASILEIRA,
        ESTRANGEIRA;
    }

    public estaAtivo getCondicao() {
        return condicao;
    }

    public void setCondicao(estaAtivo condicao) {
        this.condicao = condicao;
    }
}
