package entidades;

import java.time.LocalDate;
import java.util.UUID;

public class CartaoDeCredito {
    
    private String cvv;
    private String numero;
    private LocalDate validade;
    private UUID idCartaoDeCredito;

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return super.toString();
    }

    public String getCvv() {
        return cvv;
    }
    public void setCvv(String cvv) {
        this.cvv = cvv;
    }
    public String getNumero() {
        return numero;
    }
    public void setNumero(String numero) {
        this.numero = numero;
    }
    public LocalDate getValidade() {
        return validade;
    }
    public void setValidade(LocalDate validade) {
        this.validade = validade;
    }
    public UUID getIdCartaoDeCredito() {
        return idCartaoDeCredito;
    }
    public void setIdCartaoDeCredito(UUID idCartaoDeCredito) {
        this.idCartaoDeCredito = idCartaoDeCredito;
    }
}
